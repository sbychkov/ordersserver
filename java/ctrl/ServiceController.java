/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import ctrl.BaseCtrl;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.PriceListElement;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author serge
 */
@Controller
public class ServiceController extends BaseCtrl {

    @RequestMapping(value = "/updateDb", method = RequestMethod.GET)
    public String updateDb() {
        String result = " objects updated";
        int count = 0;
        Pageable pr = new PageRequest(0, 1000);
        Long prCount = prices.count();
        while (pr.getOffset() < prCount) {
            List<PriceListElement> ples = new ArrayList<>();
            for (PriceListElement ple : prices.findByCreateDateIsNull(pr)) {
                if (ple.getPriceList() != null && ple.getCreateDate() == null) {
                    ple.setCreateDate((Date) ple.getPriceList().getCreateDate()
                            .clone());
                    ples.add(ple);
                    ++count;
                }
            }
            prices.save(ples);
            pr = pr.next();
        }

        return count + result;
    }
}
