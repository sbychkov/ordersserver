/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import static ctrl.BaseCtrl.LOG;
import java.util.List;
import java.util.Map;
import model.Client;
import model.ClientOrder;
import model.Item;
import model.OrderElement;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author serge
 */
@RestController
public class OrderController extends BaseCtrl {

    @RequestMapping(value = "/order", method = RequestMethod.GET)
    public List<ClientOrder> getOrder() {
        return orders.findAll();
    }

    @RequestMapping(value = "/order/{id}", method = RequestMethod.GET)
    public @ResponseBody
    ClientOrder getOrderById(@PathVariable("id") Long id) {
        return orders.findOne(id);
    }

    @RequestMapping(value = "/order", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody
    ClientOrder postOrder(@RequestBody ClientOrder order) {
        ClientOrder co = null;
        LOG.info("New order " + order);
        if (order != null) {
            co = order;
            co.getSum(); //recalculate order
            co = orders.saveAndFlush(co);
        }
        return co;
    }

    // Alternate for testing
    @RequestMapping(value = "/order", method = RequestMethod.PUT, headers = "Accept=application/json")
    ClientOrder putOrder(@RequestBody Map<String, String> order) {
        ClientOrder co = null;
        LOG.info("New order put " + order);
        if (order != null) {
            co = new ClientOrder();
            orders.save(co);
            // find client id 
            co.setClient((Client) (nl(clients.findByCode(Integer.valueOf(order.get("Client"))))));
            //parse order elements
            for (String key : order.keySet()) {
                if (isNumeric(key)) {
                    int quant = Integer.valueOf(order.get(key));
                    Item item = (Item) nl(items.findByCode(Integer.valueOf(key)));
                    OrderElement oe = new OrderElement(quant, item, co);
                    co.elements.add(oe);
                }
            }
            co.getSum(); //recalculate order
            co = orders.saveAndFlush(co);
        }
        return co;
    }

}
