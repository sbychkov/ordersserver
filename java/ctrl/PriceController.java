/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Category;
import model.Item;
import model.PriceListElement;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author serge
 */
@RestController
public class PriceController extends BaseCtrl {

    /**
     *
     * Get items belongs to the category with code equals @parent
     *
     * @param parent - code of parent object
     * @return - list of items with parent equals @parent
     */
    @RequestMapping(value = "/price", method = RequestMethod.GET)
    public List<Item> getPrice(@RequestParam(required = false) Integer parent) {

        List<Item> out = new ArrayList<>();
        Category cat = (Category) nl(categories.findByCode(parent));
        for (Item item : items.findByCategory(cat)) {
            Item dto = item.toDto();
            out.add(dto);
        }
        return out;
    }

    /**
     *
     * Finds items updated since particular time
     *
     * @param lastChange - date and time of last update
     * @return list of items changed since lastChange
     */
    @RequestMapping(value = "/price", method = RequestMethod.GET, params = {"lastChange"})
    public List<Item> getLastChangedPrice(@RequestParam("lastChange") String lastChange) {
        List<Item> out = new ArrayList<>();
        for (Item item : items.findByLastChangeAfter(new Date(Long.valueOf(lastChange)))) {
            Item dto = item;
            out.add(dto);
        }
        return out;
    }

    /**
     *
     * Updates item price and quantity
     *
     * @param item - item to be updated
     * @return updated item or if item is not found null
     */
    @RequestMapping(value = "/price", method = RequestMethod.POST)
    public Item setPrice(@RequestParam("item") Item item) {
        Item dbItem = null;
        if (item != null) {
            if (item.getId() != null) {
                dbItem = items.findOne(item.getId());
            } else {
                dbItem = (Item) nl(items.findByCode(item.getCode()));
            }
            if (dbItem != null) {
                if (item.getPrice() != null && item.getQuantity() != null
                        && (item.getPrice().compareTo(dbItem.getPrice()) != 0)
                        || item.getQuantity().compareTo(dbItem.getQuantity()) != 0) {
                    dbItem.setPrice(item.getPrice());
                    dbItem.setQuantity(item.getQuantity());
                    dbItem = items.save(dbItem);
                    //Create PLE if price changed
                    prices.save(new PriceListElement(dbItem.getPrice(),
                            dbItem.getQuantity(), getCurrentPriceList(), dbItem));
                }
            }
        }
        return dbItem;
    }

    /**
     *
     * Just clear price and quantity, but not delete the item
     *
     * @param item - item to clear price and quantity
     * @return updated item or if item is not found null
     */
    @RequestMapping(value = "/price", method = RequestMethod.DELETE)
    public Item clearPrice(@RequestParam("item") Item item) {
        Item dbItem = null;
        if (item != null) {
            if (item.getId() != null) {
                dbItem = items.findOne(item.getId());
            } else {
                dbItem = (Item) nl(items.findByCode(item.getCode()));
            }
            if (dbItem != null) {
                dbItem.setPrice(BigDecimal.ZERO);
                dbItem.setQuantity(BigDecimal.ZERO);
                dbItem = items.save(dbItem);
            }
        }
        return dbItem;
    }

    /**
     * Gets an Item object by id to read the price
     *
     * @param id - object id (Long)
     * @return IItem object
     */
    @RequestMapping(value = "/price/{id}", method = RequestMethod.GET)
    public Item getPriceById(@PathVariable("id") Long id) {
        return items.findOne(id);
    }

}
