/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Item;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author serge
 */
@RestController
public class ItemController extends BaseCtrl {

    @RequestMapping(value = "/item", method = RequestMethod.GET)
    public @ResponseBody
    List<Item> getItem() {
        List<Item> result = new ArrayList<>();
        for(Item i: items.findAll()){
            result.add(i.toDto());
        }
        return result;
    }

    @RequestMapping(value = "/item", method = RequestMethod.GET, params = {"lastChange"})
    public @ResponseBody
    List<Item> getLastChange(@RequestParam String lastChange) {
        return items.findByLastChangeAfter(new Date(Long.valueOf(lastChange)));
    }

    @RequestMapping(value = "/item", method = RequestMethod.POST)
    public @ResponseBody
    Item setItem(@RequestBody Item item) {
        Item dbItem = null;
        if (item != null) {
            if (item.getId() != null) {
                dbItem = items.findOne(item.getId());
            } else {
                dbItem = (Item) nl(items.findByCode(item.getCode()));
            }
            if (dbItem != null) {
                long id = dbItem.getId();
                dbItem = item;
                dbItem.setId(id);
                dbItem = items.saveAndFlush(dbItem);
            } else {
                dbItem = items.saveAndFlush(item);
            }
        }
        return dbItem;
    }

    @RequestMapping(value = "/item", method = RequestMethod.PUT)
    public @ResponseBody
    Item createItem(@RequestBody Item item) {
        Item dbItem = null;
        if (item != null) {
            dbItem = items.save(item);
            items.flush();
        }
        return dbItem;
    }

    @RequestMapping(value = "/item", method = RequestMethod.DELETE)
    public void deleteItem(@RequestParam Long id) {
        if (id != null) {
            items.delete(id);
        }
    }

    @RequestMapping(value = "/item/{id}", method = RequestMethod.GET)
    public @ResponseBody
    Item getItemById(@PathVariable("id") Long id) {
        return items.findOne(id);
    }

    
}
