/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.Collection;
import java.util.Date;
import java.util.logging.Logger;
import model.PriceList;
import org.springframework.beans.factory.annotation.Autowired;
import repo.CategoryRepo;
import repo.ClientGroupRepo;
import repo.ClientRepo;
import repo.ItemRepo;
import repo.OrdersRepo;
import repo.PriceListRepo;
import repo.PriceRepo;

/**
 *
 * @author serge
 */
public class BaseCtrl {

    protected static final Logger LOG = Logger.getLogger(ClientController.class.getName());
    @Autowired
    protected CategoryRepo categories;
    @Autowired
    protected ClientRepo clients;
    @Autowired
    protected ClientGroupRepo groups;
    @Autowired
    protected ItemRepo items;
    @Autowired
    protected OrdersRepo orders;
    @Autowired
    protected PriceRepo prices;
    @Autowired
    private PriceListRepo pricelists;

    protected Object nl(Collection<?> c) {
        if (c != null && c.size() > 0) {
            return c.iterator().next();
        } else {
            return null;
        }
    }

    protected PriceList getCurrentPriceList() {
        @SuppressWarnings("deprecation")
        PriceList pl;
        pl = (PriceList) nl(pricelists.findByCreateDate());
        if (pl == null) {
            pl = pricelists.save(new PriceList(new Date()));
        }

        return pl;
    }

    protected static boolean isNumeric(String str) {
        NumberFormat formatter = NumberFormat.getInstance();
        ParsePosition pos = new ParsePosition(0);
        formatter.parse(str, pos);
        return str.length() == pos.getIndex();
    }

}
