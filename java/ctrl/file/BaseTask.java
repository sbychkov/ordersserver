/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl.file;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import repo.CategoryRepo;
import repo.ClientGroupRepo;
import repo.ClientRepo;
import repo.DocsRepo;
import repo.ItemRepo;
import repo.PriceListRepo;
import repo.PriceRepo;

/**
 *
 * @author 1
 */
public abstract class BaseTask implements Runnable {

    @Autowired
    protected PriceRepo prices;
    @Autowired
    protected ItemRepo items;
    @Autowired
    protected CategoryRepo cats;
    @Autowired
    protected ClientRepo clients;
    @Autowired
    protected DocsRepo docs;
    @Autowired
    protected ClientGroupRepo clientgroups;
    @Autowired
    protected PriceListRepo pricelists;
    protected final List<String[]> strings = new ArrayList<>();

    protected String replaceCommaAndSpace(String str) {
        String result = "0";
        if (str != null && !str.trim().isEmpty()) {
            result = str.replace(",", ".");
        }
        return result;
    }

    protected Integer str2int(String str) {
        Integer result = null;
        if (str != null && !str.trim().isEmpty()) {
            result = Integer.parseInt(str.replace(",", "."));
        }
        return result;
    }

    protected String date2string(Date date) {
        String result=null;
        if (date!=null){
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
         result=sdf.format(date);
        }
        return result;
    }

    protected Object nl(Collection<?> c) {
        if (c != null && c.size() > 0) {
            return c.iterator().next();
        } else {
            return null;
        }
    }
}
