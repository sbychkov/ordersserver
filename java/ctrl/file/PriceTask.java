/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl.file;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import model.Item;
import model.PriceList;
import model.PriceListElement;

/**
 *
 * @author 1
 */
public class PriceTask extends BaseTask {
    private static final Logger LOG = Logger.getLogger(PriceTask.class.getName());

    public PriceTask(List<String[]> s) {
        this.strings.addAll(s);
    }

    @Override
    public void run() {
        PriceList priceList = getCurrentPriceList();
        List<Item> itemsList = new ArrayList<>();
        for (String[] data : strings) {
            BigDecimal price = new BigDecimal(replaceCommaAndSpace(data[1]));
            BigDecimal quantity = new BigDecimal(replaceCommaAndSpace(data[2]));
            Integer code = str2int(data[0]);
            Item item = (Item) nl(items.findByCode(code));
            if (item != null && (item.getPrice() == null || (price.compareTo(item.getPrice()) != 0) || item.getQuantity() == null || (quantity.compareTo(item.getQuantity())) != 0)) {
                item.setQuantity(quantity);
                item.setPrice(price);
                itemsList.add(item);
            }
        }
        items.save(itemsList);
        LOG.info("Items updated " + itemsList.size());
        List<PriceListElement> ples = new ArrayList<>();
        for (Item item : itemsList) {
            ples.add(new PriceListElement(item.getPrice(), item.getQuantity(), priceList, item));
        }
        prices.save(ples);
        LOG.info("PLEs updated " + ples.size());
    }

    // Needed for compability with ordersTouchkit
    //
    private PriceList getCurrentPriceList() {
        @SuppressWarnings(value = "deprecation")
        PriceList pl;
        pl = (PriceList) nl(pricelists.findByCreateDate());
        if (pl == null) {
            pl = pricelists.save(new PriceList(new Date()));
        }
        return pl;
    }
    
}
