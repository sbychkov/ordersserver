package ctrl.file;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import model.Category;
import model.Item;

public class ItemTask extends BaseTask implements Runnable {

   
    private static final Logger LOG = Logger.getLogger(ItemTask.class.getName());

    public ItemTask(List<String[]> s) {
        this.strings.addAll(s);
    }

    @Override
    public void run() {
        List<Item> itemList = new ArrayList<>();

        for (String[] data : strings) {
            Category parent = null;
            if (!data[4].isEmpty()) {
                parent = (Category) nl(cats
                        .findByCode(str2int(data[4])));
                if (parent == null) {
                    parent = new Category(data[3], str2int(data[4]));
                    parent = cats.save(parent);
                }
            }

            if (data[0].equals("0")) {
                String name = data[1];
                Integer code = str2int(data[2]);
                String unit = data[5];
                Item item = (Item) nl(items.findByCode(str2int(data[2])));
                if (item == null) {
                    item = new Item(data[1], str2int(data[2]), data[5],
                            parent);
                    itemList.add(item);
                } else if (!name.equals(item.getName()) || !code.equals(item.getCode())
                        || !unit.equals(item.getUnit())) {
                    item.setName(name);
                    item.setCode(code);
                    item.setCategory(parent);
                    item.setUnit(unit);
                    itemList.add(item);
                }
            }
        }
        items.save(itemList);
        LOG.info("Items updated " + itemList.size());
    }
}
