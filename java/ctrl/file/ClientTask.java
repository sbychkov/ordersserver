/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl.file;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import model.Client;
import model.ClientGroup;

/**
 *
 * @author 1
 */
public class ClientTask extends BaseTask {

    private static final Logger LOG = Logger.getLogger(ClientTask.class.getName());

    public ClientTask(List<String[]> s) {
        strings.addAll(s);
    }

    @Override
    public void run() {
        List<Client> clientList = new ArrayList<>();
        for (String[] data : strings) {
            ClientGroup parent = null;
            Integer code = str2int(data[1]);
            String name = data[2];
            BigDecimal debt = new BigDecimal(replaceCommaAndSpace(data[3]));
            if (!data[5].isEmpty()) {
                parent = (ClientGroup) nl(clientgroups.findByCode(str2int(data[5])));
                if (parent == null) {
                    parent = new ClientGroup(data[4], str2int(data[5]));
                    parent = clientgroups.save(parent);
                }
            }
            if (data[0].equals("0")) {
                Client c;
                c = (Client) nl(clients.findByCode(str2int(data[1])));
                if (c == null) {
                    c = new Client();
                }
                if (!code.equals(c.getCode())
                        || !name.equals(c.getName())
                        || (debt.compareTo(c.getDebt()) != 0)
                        || !parent.equals(c.getParent())) {
                    c.setCode(code);
                    c.setName(name);
                    c.setDebt(debt);
                    c.setParent(parent);
                    clientList.add(c);
                }
            }
        }
        clients.save(clientList);
        LOG.info("Clients updated " + clientList.size());
    }

}
