package ctrl;

import ctrl.file.PriceTask;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Category;
import model.Client;
import model.ClientGroup;
import model.DebtDoc;
import model.Item;
import model.PriceList;
import model.PriceListElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import repo.CategoryRepo;
import repo.ClientGroupRepo;
import repo.ClientRepo;
import repo.DocsRepo;
import repo.ItemRepo;
import repo.PriceListRepo;
import repo.PriceRepo;

@RestController
public class FileController {

    private static final Logger LOG = Logger.getLogger(FileController.class
            .getName());

    @Autowired
    private PriceRepo prices;
    @Autowired
    private ItemRepo items;
    @Autowired
    private CategoryRepo cats;
    @Autowired
    private ClientRepo clients;
    @Autowired
    private DocsRepo docs;
    @Autowired
    private ClientGroupRepo clientgroups;
    @Autowired
    private PriceListRepo pricelists;

    @RequestMapping(value = "/file/{type}", method = RequestMethod.POST, headers = "content-type!=multipart/form-data")
    @ResponseBody
    public String filePrice(@PathVariable("type") String type, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String result = "Ok";
        String ipAddress = request.getHeader("HTTP_X_FORWARDED_FOR");

        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }
        LOG.info("Client " + ipAddress);
        try {
            InputStream inputStream = request.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(inputStream, Charset.forName("CP1251")));
            result += dispatch(type, bufferedReader);
        } catch (Exception e) {
            LOG.info("" + e);
            result = "Failed to upload";
        }

        return result;
    }

    @RequestMapping(value = "/file/{type}", method = RequestMethod.POST, headers = "content-type=multipart/form-data")
    @ResponseBody
    public String filePrice(@PathVariable("type") String type, @RequestParam("file") MultipartFile file, HttpServletRequest request) {
        String result = "Ok";
        String ipAddress = request.getHeader("HTTP_X_FORWARDED_FOR");

        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }
        LOG.info("Client " + ipAddress);
        if (!file.isEmpty()) {
            try {
                InputStream inputStream = file.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(
                        new InputStreamReader(inputStream, Charset.forName("CP1251")));
                result += dispatch(type, bufferedReader);
            } catch (Exception e) {
                LOG.info(e.getLocalizedMessage());
                result = "Failed to upload";
            }
        } else {
            result = "File is empty";
        }
        return result;
    }

    @RequestMapping(value = "/file", method = RequestMethod.GET)
    @ResponseBody
    public String index() {
        return "You should POST instead of GET";
    }

    private String dispatch(String type, BufferedReader br) throws Exception {
        LOG.info("Path " + type);
        String result = "";
        switch (type) {
            case "price":
                parsePrice(br);
                break;
            case "item":
                parseItem(br);
                break;
            case "category":
                parseCategory(br);
                break;
            case "debt":
                parseDebt(br);
                break;
            case "clientgroup":
                parseClientGroup(br);
                break;
            case "client":
                parseClient(br);
                break;

            default:
                result = "Path not found ";
        }

        return result;
    }

    private void parsePrice(BufferedReader br) throws Exception {
        try {
            String str;
            int count = 0;
            int err = 0;

            List<String[]> strings = new ArrayList<>();

            while ((str = br.readLine()) != null) {
                ++count;
                String[] data = str.split("\\|", -1);
                if (data.length != 4) {
                    ++err;
                } else {
                    strings.add(data);
                }
            }
            Thread t = new Thread(new PriceTask(strings));
            t.start();
            LOG.info("Strings " + count);
            LOG.info("Error strings " + err);
        } catch (Exception e) {
            LOG.info(e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    private void parseItem(BufferedReader br) throws IOException {
        String str;
        List<Item> i = new ArrayList<>();
        int count = 0;
        int err = 0;
        while ((str = br.readLine()) != null) {
            ++count;
            String[] data = str.split("\\|", -1);
            if (data.length != 7) {
                ++err;
            } else {
                Category parent = null;
                if (!data[4].isEmpty()) {
                    parent = (Category) nl(cats
                            .findByCode(str2int(data[4])));
                    if (parent == null) {
                        parent = new Category(data[3], str2int(data[4]));
                        parent = cats.save(parent);
                    }
                }

                if (data[0].equals("0")) {
                    String name = data[1];
                    Integer code = str2int(data[2]);
                    String unit = data[5];
                    Item item = (Item) nl(items.findByCode(str2int(data[2])));
                    if (item == null) {
                        item = new Item(data[1], str2int(data[2]), data[5],
                                parent);
                        i.add(item);
                    } else if (!name.equals(item.getName()) || !code.equals(item.getCode())
                            || !unit.equals(item.getUnit())) {
                        item.setName(name);
                        item.setCode(code);
                        item.setCategory(parent);
                        item.setUnit(unit);
                        i.add(item);
                    }
                }
            }
        }
        LOG.info("Strings " + count);
        LOG.info("Error strings " + err);
        items.save(i);
        LOG.info("Objects saved " + i.size());
        i = null;
        items.flush();
    }

    private void parseCategory(BufferedReader br) throws NumberFormatException, IOException {
        String str;
        int count = 0;
        int err = 0;
        while ((str = br.readLine()) != null) {
            ++count;
            String[] data = str.split("\\|", -1);
            if (data.length != 7) {
                ++err;
            } else {
                Category parent = null;
                if (data[0].equals("1")) {
                    String name = data[1];
                    Integer code = str2int(data[2]);
                    Integer parentCode = str2int(data[4]);
                    if (!data[4].isEmpty()) {
                        parent = (Category) nl(cats.findByCode(parentCode));
                        if (parent == null) {
                            parent = new Category(data[3], parentCode);
                            parent = cats.save(parent);
                        }
                    }

                    Category category = (Category) nl(cats.findByCode(code));
                    if (category == null) {
                        category = new Category(name, code, parent);
                        cats.save(category);
                    } else if (!name.equals(category.getName())
                            || !code.equals(category.getCode())
                            || !(parent != null && parent.equals(category.getParent()))) {
                        category.setName(name);
                        category.setCode(code);
                        category.setParent(parent);
                        cats.save(category);
                    }
                }
            }
        }
        LOG.info("Strings " + count);
        LOG.info("Error strings " + err);

    }

    private void parseDebt(BufferedReader br) throws NumberFormatException, IOException {
        String str;
        int count = 0;
        int err = 0;
        ArrayList<DebtDoc> dds = new ArrayList<>();
        while ((str = br.readLine()) != null) {
            ++count;
            String[] data = str.split("\\|", -1);
            Client c;
            DebtDoc dd;
            c = (Client) nl(clients.findByCode(str2int(data[0])));
            if (c != null) {
                dd = new DebtDoc();
                dd.setClient(c);
                dd.setName(data[1]);
                dd.setType(data[2]);
                dd.setDebtValue(new BigDecimal(replaceCommaAndSpace(data[3])));
                dd.setDocDate(new Date(data[4]));
                dds.add(dd);
            }

        }
        LOG.info("Strings " + count);
        LOG.info("Error strings " + err);
        docs.save(dds);
        LOG.info("Objects saved " + dds.size());
        dds = null;
    }

    private void parseClientGroup(BufferedReader br) throws IOException {
        String str;
        int count = 0;
        int err = 0;
        while ((str = br.readLine()) != null) {
            ++count;
            String[] data = str.split("\\|", -1);
            ClientGroup parent = null;
            Integer code = str2int(data[1]);
            String name = data[2];
            if (!data[5].isEmpty()) {
                parent = (ClientGroup) nl(clientgroups.findByCode(str2int(data[5])));
                if (parent == null) {
                    parent = new ClientGroup(data[4], str2int(data[5]));
                    parent = clientgroups.save(parent);
                }
            }

            if (data[0].equals("1")) {
                ClientGroup group = (ClientGroup) nl(clientgroups.findByCode(str2int(data[1])));
                if (group == null) {
                    group = new ClientGroup(name, code, parent);
                } else if (!name.equals(group.getName())
                        || !code.equals(group.getCode())
                        || !(parent != null && parent.equals(group.getParent()))) {
                    group.setName(name);
                    group.setCode(code);
                    group.setParent(parent);
                }
                clientgroups.save(group);
            }

        }

        LOG.info("Strings " + count);
        LOG.info("Error strings " + err);

    }

    private void parseClient(BufferedReader br) throws IOException {
        String str;
        int count = 0;
        int err = 0;
        ArrayList<Client> cls = new ArrayList<>();
        while ((str = br.readLine()) != null) {
            ++count;
            String[] data = str.split("\\|", -1);
            ClientGroup parent = null;
            Integer code = str2int(data[1]);
            String name = data[2];
            BigDecimal debt = new BigDecimal(replaceCommaAndSpace(data[3]));
            if (!data[5].isEmpty()) {
                parent = (ClientGroup) nl(clientgroups.findByCode(str2int(data[5])));
                if (parent == null) {
                    parent = new ClientGroup(data[4], str2int(data[5]));
                    parent = clientgroups.save(parent);
                }
            }
            if (data[0].equals("0")) {
                Client c;
                c = (Client) nl(clients.findByCode(str2int(data[1])));
                if (c == null) {
                    c = new Client();
                }
                if (!code.equals(c.getCode())
                        || !name.equals(c.getName())
                        || (debt.compareTo(c.getDebt()) != 0)
                        || !parent.equals(c.getParent())) {
                    c.setCode(code);
                    c.setName(name);
                    c.setDebt(debt);
                    c.setParent(parent);
                    cls.add(c);
                }
            }
        }

        LOG.info("Strings " + count);
        LOG.info("Error strings " + err);
        clients.save(cls);
        LOG.info("Objects saved " + cls.size());
        cls = null;
    }

    String replaceCommaAndSpace(String str) {
        String result = "0";
        if (str != null && !str.trim().isEmpty()) {
            result = str.replace(",", ".");
        }
        return result;
    }

    private Integer str2int(String str) {
        Integer result = null;
        if (str != null && !str.trim().isEmpty()) {
            result = Integer.parseInt(str.replace(",", "."));
        }
        return result;
    }

    private String date2string(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        return sdf.format(date);
    }

    private Object nl(Collection<?> c) {
        if (c != null && c.size() > 0) {
            return c.iterator().next();
        } else {
            return null;
        }
    }

   


}
