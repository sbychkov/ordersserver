/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import dto.IClient;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Client;
import model.ClientGroup;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClientController extends BaseCtrl {

    @RequestMapping(value = "/client", method = RequestMethod.GET)
    public List<IClient> getClient() {
        List<IClient> out = new ArrayList<>();
        for(ClientGroup g:groups.findAll()){
        out.add(g.toDto());
        }
        for(Client c:clients.findAll()){
            out.add(c.toDto());
        }
        return out;
    }

    @RequestMapping(value = "/client", method = RequestMethod.GET, params = {"lastChange"})
    public List<Client> getLastChanges(@RequestParam String lastChange) {
        return clients.findByLastChangeAfter(new Date(Long.valueOf(lastChange)));
    }

    @RequestMapping(value = "/client", method = RequestMethod.POST)
    public Client setClient(@RequestParam Client client) {
        Client dbClient = getDbClient(client);
        if (dbClient != null) {
            Long id = dbClient.getId();
            dbClient = client;
            dbClient.setId(id);
            dbClient = clients.save(dbClient);
        } else {
            dbClient = client;
            dbClient = clients.save(dbClient);
        }
        return dbClient;
    }

    @RequestMapping(value = "/client", method = RequestMethod.PUT)
    public Client createClient(@RequestParam Client client) {
        Client dbClient = null;
        if (client != null) {
            dbClient = client;
            dbClient = clients.save(dbClient);
        }
        return dbClient;
    }

    @RequestMapping(value = "/client", method = RequestMethod.DELETE)
    public void deleteClient(@RequestParam Long id) {
        if (id != null) {
            clients.delete(id);
        }
    }

    private Client getDbClient(Client client) {
        Client dbClient = null;
        if (client != null) {
            if (client.getId() != null) {
                dbClient = clients.findOne(client.getId());
            } else if (client.getCode() != null) {
                dbClient = (Client) nl(clients.findByCode(client.getCode()));
            }
        }
        return dbClient;
    }
    /*
    @RequestMapping(value = "/client", method = RequestMethod.GET)
    public List<Client> getClient(@RequestParam(required = true) Integer parent) {

        List<Client> out = new ArrayList<>();
        ClientGroup gr =  (ClientGroup) nl(groups.findByCode(parent));
        for (Client client : clients.findByParent(gr)) {
            Client dto = client.toDto();
            out.add(dto);
        }
        return out;
    }*/
}
