package orders;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import java.util.Properties;
import javax.sql.DataSource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.EclipseLinkJpaVendorAdapter;

@Configuration
@ComponentScan(basePackages = "ctrl")
@EnableAutoConfiguration
@EnableJpaRepositories(basePackages = "repo", considerNestedRepositories = true)

public class OrdersServerApplication  {
    
    public static void main(String[] args) {
        
        SpringApplication.run(OrdersServerApplication.class, args);
        
    }
    
    //@Override
    //protected void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
      //  super.configureRepositoryRestConfiguration(config);
        //config.setReturnBodyOnCreate(true);
        //config.setDefaultMediaType(MediaType.parseMediaType(MediaType.APPLICATION_JSON_VALUE));
        //config.useHalAsDefaultJsonMediaType(false);
   // }

    /*@Bean
     public Jackson2ObjectMapperBuilder jacksonBuilder() {
     Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
     builder.indentOutput(true).dateFormat(new SimpleDateFormat("yyyy-MM-dd"));
     return builder;
     }
    
     @Bean
     public ObjectMapper jacksonObjectMapper() {
     return new ResourcesMapper();
     }*/
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        EclipseLinkJpaVendorAdapter vendorAdapter = new EclipseLinkJpaVendorAdapter();
        vendorAdapter.setGenerateDdl(true);
        vendorAdapter.setShowSql(false);
        
        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setPackagesToScan("model");
        factory.setDataSource(dataSource());
        factory.setJpaProperties(jpaProperties());
        //factory.setLoadTimeWeaver(new TomcatLoadTimeWeaver());
        return factory;
    }
    
    @Bean
    public JpaTransactionManager transactionManager() {
        JpaTransactionManager txManager = new JpaTransactionManager();
        txManager.setEntityManagerFactory(entityManagerFactory().getObject());
        return txManager;
    }
    
    @Bean
    public PersistenceExceptionTranslationPostProcessor persistenceExceptionTranslationPostProcessor() {
        return new PersistenceExceptionTranslationPostProcessor();
    }
    
    @Bean
    public DataSource dataSource() {
        MysqlDataSource mds = new MysqlDataSource();
        mds.setURL("jdbc:mysql://127.6.225.2:3306/orders?zeroDateTimeBehavior=convertToNull&characterEncoding=utf8");
        mds.setUser("ordersUser");
        mds.setPassword("1q2w3e4r");
        return mds;
    }
    
    private Properties jpaProperties() {
        Properties properties = new Properties();
        properties.setProperty("eclipselink.ddl-generation", "create-or-extend-tables");
        properties.setProperty("eclipselink.ddl-generation.index-foreign-keys", "true");
        properties.setProperty("eclipselink.query-results-cache", "true");
        properties.setProperty("exclude-unlisted-classes", "false");
        properties.setProperty("eclipselink.weaving", "static");
        return properties;
    }
}
