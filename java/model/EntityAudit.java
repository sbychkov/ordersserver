/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author serge
 */
public interface EntityAudit extends EntityId {

    public Date getCreateDate();

    public void setCreateDate(Date date);

    public Date getLastChange();

    public void setLastChange(Date date);

    void precreate();

    void preedit();
}
