/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import dto.ICategory;
import dto.IItem;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement

public class Item implements Serializable, EntityAudit, Entity1c {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    private Long id;
    @Column(length = 255)
    private String name;
    @Column
    private Integer code;
    @Column
    private String unit;
    //@JsonBackReference
    @JoinColumn(name = "Category", referencedColumnName = "ID")
    @ManyToOne
    private Category category;
    @Column(precision = 38, scale = 2)
    private BigDecimal price;
    @Column(precision = 38, scale = 3)
    private BigDecimal quantity;
    @JsonIgnore
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @JsonIgnore
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastChange;

    /**
     *
     */
    public Item() {
    }

    public Item(String name, Integer code, String unit, Category category) {
        this.name = name;
        this.code = code;
        this.unit = unit;
        this.category = category;
    }

    public Item(String name, Integer code, String unit, Category category, BigDecimal price, BigDecimal quantity) {
        this.name = name;
        this.code = code;
        this.unit = unit;
        this.category = category;
        this.price = price;
        this.quantity = quantity;
    }

    public Item(String name, Integer code, String unit, BigDecimal price, BigDecimal quantity) {
        this.name = name;
        this.code = code;
        this.unit = unit;
        this.price = price;
        this.quantity = quantity;
    }

    public Item(String name, Integer code, String unit) {
        this.name = name;
        this.code = code;
        this.unit = unit;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the category
     */
    @JsonIgnore
    public Category getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    @JsonIgnore
    public void setCategory(Category category) {
        this.category = category;
    }

    /**
     * @return the code
     */
    @Override
    public Integer getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    @Override
    public void setCode(Integer code) {
        this.code = code;
    }

    /**
     * @return the unit
     */
    public String getUnit() {
        return unit;
    }

    /**
     * @param unit the unit to set
     */
    public void setUnit(String unit) {
        this.unit = unit;
    }

    @PrePersist
    @Override
    public void precreate() {
        createDate = new Date();
    }

    @PreUpdate
    @Override
    public void preedit() {

        lastChange = new Date();
        if (createDate == null) {
            createDate = lastChange;
        }
    }

    /**
     * @return the createDate
     */
    @Override
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate the createDate to set
     */
    @Override
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the lastChange
     */
    @Override
    public Date getLastChange() {
        return lastChange;
    }

    /**
     * @param lastChange the lastChange to set
     */
    @Override
    public void setLastChange(Date lastChange) {
        this.lastChange = lastChange;
    }

    /**
     * @return the price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * @return the quantity
     */
    public BigDecimal getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Item{" + "id=" + id + ", name=" + name + ", code=" + code + ", unit=" + unit + ", category=" + category + ", price=" + price + ", quantity=" + quantity + ", createDate=" + createDate + ", lastChange=" + lastChange + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.id);
        hash = 37 * hash + Objects.hashCode(this.name);
        hash = 37 * hash + Objects.hashCode(this.code);
        hash = 37 * hash + Objects.hashCode(this.unit);
        hash = 37 * hash + Objects.hashCode(this.category);
        hash = 37 * hash + Objects.hashCode(this.price);
        hash = 37 * hash + Objects.hashCode(this.quantity);
        hash = 37 * hash + Objects.hashCode(this.createDate);
        hash = 37 * hash + Objects.hashCode(this.lastChange);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Item other = (Item) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        if (!Objects.equals(this.unit, other.unit)) {
            return false;
        }
        if (!Objects.equals(this.category, other.category)) {
            return false;
        }
        if (!Objects.equals(this.price, other.price)) {
            return false;
        }
        return Objects.equals(this.quantity, other.quantity);
    }

    public Item toDto() {
        Item result = new Item(name, code, unit, category, price, quantity);
        if (category != null) {
            result.setParent(new Category(category.getName(), category.getCode()));
        }
        return result;
    }

    public Category getParent() {
        return category;
    }

    public void setParent(Category parent) {
        this.category = parent;
    }
}
