/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import dto.IClient;
import javax.persistence.FetchType;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@XmlRootElement
public class Client implements Serializable, EntityAudit, Entity1c, IClient {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    private Long id;
    @Column(length = 255)
    private String name;
    private Integer code;
    @JsonManagedReference(value = "client-order")
    @OneToMany(mappedBy = "client", fetch = FetchType.LAZY)
    private List<ClientOrder> clientOrderList;
    private BigDecimal debt;
    @JsonManagedReference(value = "client-doc")
    @OneToMany(mappedBy = "client", orphanRemoval = true, fetch = FetchType.LAZY)
    private List<DebtDoc> debtDocList;
    //@JsonBackReference(value ="parent")
    @ManyToOne
    @JoinColumn(name = "ClientGroup", referencedColumnName = "ID")
    private ClientGroup parent;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastChange;

    public Client() {
    }

    public Client(String name) {
        this.name = name;
    }

    public Client(String name, Integer code) {
        this.name = name;
        this.code = code;
    }

    @JsonIgnore
    public List<ClientOrder> getClientOrderList() {
        return clientOrderList;
    }

    @JsonIgnore
    public void setClientOrderList(List<ClientOrder> clientOrderList) {
        this.clientOrderList = clientOrderList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonIgnore
    @XmlTransient
    public List<ClientOrder> getClientorderList() {
        return clientOrderList;
    }

    @JsonIgnore
    public void setClientorderList(List<ClientOrder> clientorderList) {
        this.clientOrderList = clientorderList;
    }

    @Override
    public String toString() {
        return "Client[ id=" + id + " ]";
    }

    public Date getTimeStamp() {
        return new Date();
    }

    public Location getLocation() {
        return new Location();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Client other = (Client) obj;
        if ((this.name == null) ? (other.name != null) : !this.name
                .equals(other.name)) {
            return false;
        }
        return true;
    }

    /**
     * @return the code
     */
    public Integer getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(Integer code) {
        this.code = code;
    }

    /**
     * @return the debt
     */
    public BigDecimal getDebt() {
        return debt;
    }

    /**
     * @param debt the debt to set
     */
    public void setDebt(BigDecimal debt) {
        this.debt = debt;
    }

    /**
     * @return the debtDocList
     */
    public List<DebtDoc> getDebtDocList() {
        return debtDocList;
    }

    /**
     * @param debtDocList the debtDocList to set
     */
    public void setDebtDocList(List<DebtDoc> debtDocList) {
        this.debtDocList = debtDocList;
    }

    /**
     * @return the parent
     */
    @Override
    public ClientGroup getParent() {
        return parent;
    }

    /**
     * @param parent the parent to set
     */
    @Override
    public void setParent(ClientGroup parent) {
        this.parent = parent;
    }

    @PrePersist
    @Override
    public void precreate() {
        createDate = new Date();
    }

    @PreUpdate
    @Override
    public void preedit() {
        lastChange = new Date();
    }

    /**
     * @return the createDate
     */
    @Override
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate the createDate to set
     */
    @Override
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the lastChange
     */
    @Override
    public Date getLastChange() {
        return lastChange;
    }

    /**
     * @param lastChange the lastChange to set
     */
    @Override
    public void setLastChange(Date lastChange) {
        this.lastChange = lastChange;
    }

    @Override
    public boolean isCategory() {
        return false;
    }
    
    public Client toDto() {
        Client result = new  Client(name, code);
        result.setDebt(debt);
        if (parent != null){
            result.setParent(new ClientGroup(parent.getName(), parent.getCode()));
        }
        return result;
    }

}
