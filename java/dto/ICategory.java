/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import model.Category;

/**
 *
 * @author serge
 */
public interface ICategory extends IItem{
    
    Integer getCode();

    Long getId();

    String getName();

    ICategory getParent();

    void setCode(Integer code);

    void setId(Long id);

    void setName(String name);

    void setParent(Category parent);
    
    boolean isCategory();
    
}
