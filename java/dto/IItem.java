/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import java.math.BigDecimal;
import model.Category;

/**
 *
 * @author serge
 */
public interface IItem {

    Integer getCode();

    Long getId();

    String getName();

    ICategory getParent();

    BigDecimal getPrice();

    BigDecimal getQuantity();

    String getUnit();

    boolean isCategory();
    
    void setCategory(boolean category);

    void setCode(Integer code);

    void setId(Long id);

    void setName(String name);

    void setParent(Category parent);

    void setPrice(BigDecimal price);

    void setQuantity(BigDecimal quantity);

    void setUnit(String unit);
    
}
