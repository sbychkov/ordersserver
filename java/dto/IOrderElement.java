/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import java.math.BigDecimal;
import model.Item;

/**
 *
 * @author serge
 */
public interface IOrderElement {

    public Item getItem();

    public BigDecimal getPrice();

    public Integer getQuantity();

    public void setItem(Item item);

    public void setPrice(BigDecimal price);

    public void setQuantity(Integer quantity);

    public IOrder getClientOrder();

    public void setClientOrder(IOrder clientOrder);
    
    
    
}
