/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import model.Client;
import model.OrderElement;

/**
 *
 * @author serge
 */
public interface IOrder {

    public Client getClient();

    public Date getCreateDate();

    public Collection<OrderElement> getElements();

    public Long getId();

    public Long getRegId();

    public BigDecimal getSum();

    public void setClient(Client client);

    public void setCreateDate(Date createDate);

    public void setElements(Collection<OrderElement> elements);

    public void setId(Long id);

    public void setRegId(Long regId);

    public void setSum(BigDecimal sum);

}
