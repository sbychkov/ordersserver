/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import model.ClientGroup;

/**
 *
 * @author serge
 */
public interface IClientGroup extends IClient{
 
    @Override
    Integer getCode();

    @Override
    Long getId();

    @Override
    String getName();

    @Override
    ClientGroup getParent();
   
    @Override
    void setCode(Integer code);

    @Override
    void setId(Long id);

    @Override
    void setName(String name);

    @Override
    void setParent(ClientGroup parent);
    
}
