/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repo;

import java.util.Date;
import java.util.List;
import model.Category;
import model.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author serge
 */
@RepositoryRestResource(collectionResourceRel = "items", path = "items")
public interface ItemRepo extends JpaRepository<Item, Long> {

    @ResponseBody
    List<Item> findByName(@Param("name") String name);

    @ResponseBody
    List<Item> findByCode(@Param("code") Integer code);

    @ResponseBody
    List<Item> findByCategory(@Param("category") Category category);

    @ResponseBody
    List<Item> findByLastChangeAfter(@Param("lastChange") Date lastChange);
}
