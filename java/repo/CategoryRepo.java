package repo;

import java.util.Date;
import java.util.List;
import model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "itemgroups", path = "itemgroups")
public interface CategoryRepo extends JpaRepository<Category, Long> {

    public List<Category> findByCode(Integer code);

    public List<Category> findByParent(Category parent);

    public List<Category> findByLastChangeAfter(Date lastChange);
}
