/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repo;

import java.util.Date;
import java.util.List;
import model.Client;
import model.ClientOrder;
import model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author serge
 */
@RepositoryRestResource(collectionResourceRel = "orders", path = "orders")
public interface OrdersRepo extends JpaRepository<ClientOrder, Long> {

    


    public List<ClientOrder> findByClient(@Param("client") Client client);

    public List<ClientOrder> findByUser(@Param("user") User user);

    public List<ClientOrder> findByCreateDate(@Param("createdate") Date createDate);

    public List<ClientOrder> findByEstimateDate(
            @Param("estimatedate") Date estimateDate);

    public List<ClientOrder> findByCreateDateAndUser(
            @Param("createdate") Date createDate, @Param("user") User user);

    public List<ClientOrder> findByEstimateDateAndUser(
            @Param("estimatedate") Date estimateDate, @Param("user") User user);
}
