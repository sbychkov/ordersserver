/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repo;

import java.util.List;
import model.PriceList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author serge
 */
//@RepositoryRestResource(collectionResourceRel = "prices", path = "prices")
public interface PriceListRepo extends JpaRepository<PriceList, Long> {

    @Query(value = "SELECT * FROM `PRICELIST` p where p.`CREATEDATE`=CURDATE()", nativeQuery = true)
    public List<PriceList> findByCreateDate();
}
