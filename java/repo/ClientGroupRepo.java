package repo;

import java.util.List;
import model.ClientGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "clientgroups", path = "clientgroups")
public interface ClientGroupRepo extends JpaRepository<ClientGroup, Long> {
    public List<ClientGroup> findByCode(Integer code);    
}
