package repo;

import java.util.List;
import model.Item;
import model.PriceList;
import model.PriceListElement;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PriceRepo extends JpaRepository<PriceListElement, Long> {

    public List<PriceListElement> findByItem(Item item);

    // @Query(value="SELECT * FROM PRICELISTELEMENT WHERE ITEM = ?1 AND PRICELIST IN (SELECT pricelist FROM PRICELISTELEMENT WHERE ITEM = ?1 ORDER BY PRICELIST DESC, LIMIT 1)")
    @Query("SELECT p FROM PriceListElement p WHERE p.item=?1 ORDER BY p.createDate DESC")
    public List<PriceListElement> latestPrice(Item item);

    @Query(value = "SELECT p.`ID`,p.`PRICE`,p.`QUANTITY`,p.`ITEM`, p.`PRICELIST`, p.`CREATEDATE`, p.`LASTCHANGE` FROM  `PRICELISTELEMENT` p, `ITEM` i WHERE i.`CODE` = ?1 AND p.`ITEM` = i.`ID` GROUP BY p.`ITEM` HAVING MAX(p.`CREATEDATE`)", nativeQuery = true)
    public List<PriceListElement> latestPrice(Integer code);

    public PriceList findPriceListByItem(Item item);

    public List<PriceListElement> findByPriceIsNotNull();

    @Query(value = "SELECT `ID`,`PRICE`,`QUANTITY`,`ITEM`, `PRICELIST`, `CREATEDATE`, `LASTCHANGE` FROM  `PRICELISTELEMENT` GROUP BY `ITEM` HAVING MAX(  `CREATEDATE`)", nativeQuery = true)
    public List<PriceListElement> nativeSelect();

    @Query(value = "SELECT `ID`,`PRICE`,`QUANTITY`,`ITEM`, `PRICELIST`, `CREATEDATE`, `LASTCHANGE` FROM  `PRICELISTELEMENT` GROUP BY `ITEM` HAVING MAX(  `CREATEDATE`), LIMIT ?1, ?2", nativeQuery = true)
    public List<PriceListElement> nativeSelect(int start, int end);

    @Query(value = "SELECT `ID`,`PRICE`,`QUANTITY`,`ITEM`, `PRICELIST`, `CREATEDATE`, `LASTCHANGE` FROM  `PRICELISTELEMENT` WHERE `ITEM` = ?1 GROUP BY `ITEM` HAVING MAX(`CREATEDATE`)", nativeQuery = true)
    public List<PriceListElement> nativeSelect(Long item);

    public List<PriceListElement> findByCreateDateIsNull(Pageable p);

}
