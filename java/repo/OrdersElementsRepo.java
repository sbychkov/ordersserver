/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repo;

import model.OrderElement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author serge
 */
@RepositoryRestResource(collectionResourceRel = "orderelements", path = "orderelements")
public interface OrdersElementsRepo extends JpaRepository<OrderElement, Long> {

}
