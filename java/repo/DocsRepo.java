package repo;

import model.DebtDoc;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "debtdocs", path = "debtdocs")
public interface DocsRepo extends JpaRepository<DebtDoc,Long>{

}
