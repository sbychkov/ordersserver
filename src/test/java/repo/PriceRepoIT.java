/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repo;

import java.util.List;
import model.Item;
import model.PriceList;
import model.PriceListElement;
import orders.OrdersServerApplication;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author serge
 */
@ContextConfiguration(classes = {OrdersServerApplication.class})
@RunWith(SpringJUnit4ClassRunner.class)
public class PriceRepoIT {

    @Autowired
    PriceRepo instance;

    public PriceRepoIT() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of findByItem method, of class PriceRepo.
     */
    @Test
    public void testFindByItem() {
        System.out.println("findByItem");
        Item item = null;

        List<PriceListElement> expResult = null;
        List<PriceListElement> result = instance.findByItem(item);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of latestPrice method, of class PriceRepo.
     */
    @Test
    public void testLatestPrice_Item() {
        System.out.println("latestPrice");
        Item item = null;

        List<PriceListElement> expResult = null;
        List<PriceListElement> result = instance.latestPrice(item);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of latestPrice method, of class PriceRepo.
     */
    @Test
    public void testLatestPrice_Integer() {
        System.out.println("latestPrice");
        Integer code = null;

        List<PriceListElement> expResult = null;
        List<PriceListElement> result = instance.latestPrice(code);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of findPriceListByItem method, of class PriceRepo.
     */
    @Test
    public void testFindPriceListByItem() {
        System.out.println("findPriceListByItem");
        Item item = null;

        PriceList expResult = null;
        PriceList result = instance.findPriceListByItem(item);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of findByPriceIsNotNull method, of class PriceRepo.
     */
    @Test
    public void testFindByPriceIsNotNull() {
        System.out.println("findByPriceIsNotNull");

        List<PriceListElement> expResult = null;
        List<PriceListElement> result = instance.findByPriceIsNotNull();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of nativeSelect method, of class PriceRepo.
     */
    @Test
    public void testNativeSelect_0args() {
        System.out.println("nativeSelect");

        List<PriceListElement> expResult = null;
        List<PriceListElement> result = instance.nativeSelect();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of nativeSelect method, of class PriceRepo.
     */
    @Test
    public void testNativeSelect_int_int() {
        System.out.println("nativeSelect");
        int start = 0;
        int end = 0;

        List<PriceListElement> expResult = null;
        List<PriceListElement> result = instance.nativeSelect(start, end);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of nativeSelect method, of class PriceRepo.
     */
    @Test
    public void testNativeSelect_Long() {
        System.out.println("nativeSelect");
        Long item = null;

        List<PriceListElement> expResult = null;
        List<PriceListElement> result = instance.nativeSelect(item);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of findByCreateDateIsNull method, of class PriceRepo.
     */
    @Test
    public void testFindByCreateDateIsNull() {
        System.out.println("findByCreateDateIsNull");
        Pageable p = null;

        List<PriceListElement> expResult = null;
        List<PriceListElement> result = instance.findByCreateDateIsNull(p);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }

   

}
