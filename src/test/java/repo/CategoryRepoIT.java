/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repo;

import java.util.Date;
import java.util.List;
import model.Category;
import orders.OrdersServerApplication;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author serge
 */
@ContextConfiguration(classes = {OrdersServerApplication.class})
@RunWith(SpringJUnit4ClassRunner.class)
public class CategoryRepoIT {

    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Autowired
    CategoryRepo instance;
    
    public CategoryRepoIT() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of findByCode method, of class CategoryRepo.
     */
    @Test
    public void testFindByCode() {
        System.out.println("findByCode");
        Integer code = null;
        List<Category> expResult = null;
        List<Category> result = instance.findByCode(code);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
    }

    /**
     * Test of findByParent method, of class CategoryRepo.
     */
    @Test
    public void testFindByParent() {
        System.out.println("findByParent");
        Category parent = null;
        List<Category> expResult = null;
        List<Category> result = instance.findByParent(parent);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    
    }

    /**
     * Test of findByLastChangeAfter method, of class CategoryRepo.
     */
    @Test
    public void testFindByLastChangeAfter() {
        System.out.println("findByLastChangeAfter");
        Date lastChange = new Date();
      
        List<Category> expResult = null;
        List<Category> result = instance.findByLastChangeAfter(lastChange);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
    }

        
    
}
