/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repo;

import java.util.Date;
import java.util.List;
import model.Client;
import model.ClientOrder;
import model.User;
import orders.OrdersServerApplication;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author serge
 */
@ContextConfiguration(classes = {OrdersServerApplication.class})
@RunWith(SpringJUnit4ClassRunner.class)
public class OrdersRepoIT {

    @Autowired
    OrdersRepo instance;

    public OrdersRepoIT() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of findByClient method, of class OrdersRepo.
     */
    @Test
    public void testFindByClient() {
        System.out.println("findByClient");
        Client client = null;

        List<ClientOrder> expResult = null;
        List<ClientOrder> result = instance.findByClient(client);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of findByUser method, of class OrdersRepo.
     */
    @Test
    public void testFindByUser() {
        System.out.println("findByUser");
        User user = null;

        List<ClientOrder> expResult = null;
        List<ClientOrder> result = instance.findByUser(user);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of findByCreateDate method, of class OrdersRepo.
     */
    @Test
    public void testFindByCreateDate() {
        System.out.println("findByCreateDate");
        Date createDate = null;

        List<ClientOrder> expResult = null;
        List<ClientOrder> result = instance.findByCreateDate(createDate);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of findByEstimateDate method, of class OrdersRepo.
     */
    @Test
    public void testFindByEstimateDate() {
        System.out.println("findByEstimateDate");
        Date estimateDate = null;

        List<ClientOrder> expResult = null;
        List<ClientOrder> result = instance.findByEstimateDate(estimateDate);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of findByCreateDateAndUser method, of class OrdersRepo.
     */
    @Test
    public void testFindByCreateDateAndUser() {
        System.out.println("findByCreateDateAndUser");
        Date createDate = null;
        User user = null;

        List<ClientOrder> expResult = null;
        List<ClientOrder> result = instance.findByCreateDateAndUser(createDate, user);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of findByEstimateDateAndUser method, of class OrdersRepo.
     */
    @Test
    public void testFindByEstimateDateAndUser() {
        System.out.println("findByEstimateDateAndUser");
        Date estimateDate = null;
        User user = null;

        List<ClientOrder> expResult = null;
        List<ClientOrder> result = instance.findByEstimateDateAndUser(estimateDate, user);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of findByUploadedTrue method, of class OrdersRepo.
     */
    @Test
    public void testFindByUploadedTrue() {
        System.out.println("findByUploadedTrue");
       
        List<ClientOrder> expResult = null;
        List<ClientOrder> result = instance.findByUploadedTrue();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
    }

    /**
     * Test of findByUploadedFalse method, of class OrdersRepo.
     */
    @Test
    public void testFindByUploadedFalse() {
        System.out.println("findByUploadedFalse");
     
        List<ClientOrder> expResult = null;
        List<ClientOrder> result = instance.findByUploadedFalse();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
    }

   

}
