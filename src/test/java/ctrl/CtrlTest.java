/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;
import java.util.logging.Logger;
import model.ClientOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import repo.ClientRepo;
import repo.ItemRepo;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.Converter;
import retrofit.converter.GsonConverter;
import retrofit.converter.JacksonConverter;

/**
 *
 * @author serge
 */
@SuppressWarnings("serial")
public class CtrlTest {

    @Autowired
    protected ClientRepo clients;
    @Autowired
    protected ItemRepo items;

    protected static final Logger LOG = Logger.getLogger(CtrlTest.class.getName());

    protected static final String SERVER = "http://172.16.1.22:8080";

    protected final Gson gson = (new GsonBuilder())
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
            .create();

    private RequestInterceptor requestInterceptor = new RequestInterceptor() {
        @Override
        public void intercept(RequestFacade request) {
            request.addHeader("User-Agent", "Orders Test Client");
            request.addHeader("Accept", "application/json");
        }
    };
    protected CtrlApi ctrlSvc = new RestAdapter.Builder()
            .setEndpoint(SERVER)
            .setClient(new OkClient(new OkHttpClient()))
            .setConverter(new GsonConverter(gson))
            .setRequestInterceptor(requestInterceptor)
            .setLogLevel(RestAdapter.LogLevel.FULL)
            //.setConverter(new JacksonConverter(new ResourcesMapper()))
            .build()
            .create(CtrlApi.class);

    @Test
    public void testCtrlSvc() {
        System.out.println("testCtrlSvc");
        assertTrue(ctrlSvc != null);
        ClientOrder random = TestData.randomOrder();
        try {
            ClientOrder o1 = gson.fromJson(TestData.toJson(random), ClientOrder.class);
            ClientOrder o2 = (ClientOrder) TestData.fromJson(gson.toJson(random), ClientOrder.class);
            LOG.info(o1.toString());
            LOG.info(o2.toString());
            assertEquals(random, o1);
            assertEquals(random, o2);
        } catch (Exception ex) {
            LOG.severe(ex.getLocalizedMessage());
        }
    }

}
