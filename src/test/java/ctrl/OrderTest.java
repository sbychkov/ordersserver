/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import model.Client;
import model.ClientOrder;
import model.Item;
import model.OrderElement;
import orders.OrdersServerApplication;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import retrofit.RetrofitError;

/**
 *
 * @author serge
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {OrdersServerApplication.class})
public class OrderTest extends CtrlTest {

    /*@Test
    public void testPreparedOrder() throws Exception {
        String jsonString = "{\"id\":41751,\"createDate\":\"2015-01-22T21:00:00Z\",\"estimateDate\":null,\"elements\":[{\"id\":41752,\"quantity\":5,\"item\":{\"id\":2676,\"name\":\"\u042d\u0440\u0430 \u0421\u0442\u0430\u0431\u0438\u043b\u0438\u0437\u0430\u0442\u043e\u0440 STA -1500\",\"code\":20154,\"unit\":\"\u0448\u0442.\",\"price\":1416.15,\"quantity\":3.000,\"parent\":{\"id\":120,\"name\":\"\u0421\u0442\u0430\u0431\u0438\u043b\u0438\u0437\u0430\u0442\u043e\u0440\u044b\",\"code\":20151,\"children\":[],\"category\":true,\"unit\":null,\"price\":null,\"quantity\":null}},\"elementSum\":0,\"elementPrice\":null,\"price\":null}],\"uploaded\":false,\"executed\":false,\"orderSum\":null,\"docNum\":null,\"regId\":41751}";
        ClientOrder order = gson.fromJson(jsonString, ClientOrder.class);
        //ctrlSvc.addOrder(order);
        //Collection<ClientOrder> stored = ctrlSvc.getOrderList();
        //assertTrue(stored.contains(order));
        order = (ClientOrder) TestData.fromJson(jsonString, ClientOrder.class);
        //ctrlSvc.addOrder(order);
        assertEquals(gson.toJson(order), jsonString);
    }*/

    @Test
    public void testFetchAllOrder() {
        try {
            List<ClientOrder> list = (List<ClientOrder>) ctrlSvc.getOrderList();
            assertTrue(list != null);
        } catch (RetrofitError re) {
            // re.printStackTrace();
            CtrlTest.LOG.info(re.getUrl());
            CtrlTest.LOG.info(re.getBody().toString());
            CtrlTest.LOG.info(re.getResponse().toString());
            fail(re.getLocalizedMessage());
        }
    }

    @Test
    public void testAddOrderMetadata() throws Exception {
        try {
            ClientOrder random = generateOrder();
            Long id = (ctrlSvc.addOrder(random)).getId();
            ClientOrder received = ctrlSvc.getOrderById(id);
            assertEquals(random.getClient(), received.getClient());
            assertEquals(random.getCreateDate(), received.getCreateDate());
            assertEquals(random.getEstimateDate(), received.getEstimateDate());
            assertEquals(random.getUser(), received.getUser());
            assertEquals(random.getSum(), received.getSum());
            assertTrue(received.getId() > 0);
            assertEquals(received.getId(), id);
        } catch (RetrofitError re) {
            //   re.printStackTrace();
            CtrlTest.LOG.info(re.getUrl());
            CtrlTest.LOG.info(re.getBody().toString());
            CtrlTest.LOG.info(re.getResponse().toString());
            fail(re.getLocalizedMessage());
        }
    }

    @Test
    public void testAddGetOrder() throws Exception {
        try {
            ClientOrder order = generateOrder();
            CtrlTest.LOG.info(gson.toJson(order));
            CtrlTest.LOG.info(TestData.toJson(order));
            CtrlTest.LOG.info((gson.fromJson(gson.toJson(order), ClientOrder.class)).toString());
            ctrlSvc.addOrder(order);
            Collection<ClientOrder> stored = ctrlSvc.getOrderList();
            assertTrue(stored.contains(order));
        } catch (RetrofitError re) {
            //    re.printStackTrace();
            CtrlTest.LOG.info(re.getUrl());
            CtrlTest.LOG.info(re.getBody().toString());
            CtrlTest.LOG.info(re.getResponse().toString());
            fail(re.getLocalizedMessage());
        }
    }

    @Test
    public void testGetNonExistantVideosData() throws Exception {
        long nonExistantId = getInvalidOrderId();
        try {
            ClientOrder response = ctrlSvc.getOrderById(nonExistantId);
            assertTrue(response == null);
        } catch (RetrofitError re) {
            //      re.printStackTrace();
            CtrlTest.LOG.info(re.getUrl());
            CtrlTest.LOG.info(re.getBody().toString());
            CtrlTest.LOG.info(re.getResponse().toString());
            fail(re.getLocalizedMessage());
        }
    }

    private long getInvalidOrderId() {
        Set<Long> ids = new HashSet<>();
        Collection<ClientOrder> stored = ctrlSvc.getOrderList();
        for (ClientOrder v : stored) {
            ids.add(v.getId());
        }
        long nonExistantId = Long.MIN_VALUE;
        while (ids.contains(nonExistantId)) {
            nonExistantId++;
        }
        return nonExistantId;
    }

    private ClientOrder generateOrder() {
        ClientOrder result;

        // Get random Client and Item
        int max = (int) clients.count();
        Random r = new Random();
        int clientPos = r.nextInt(max); // Position in Client list
        max = (int) items.count();
        int itemPos = r.nextInt(max);// Position in Item list
        Client client = clients.findAll().get(clientPos);
        Item item = items.findAll().get(itemPos);
        // Create Order
        result = new ClientOrder(new Date(), new Date(), null, client);
        result.elements.add(new OrderElement(r.nextInt(100), item, result));
        return result;
    }

}
