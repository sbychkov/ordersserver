/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import java.util.Collection;
import java.util.List;
import model.Item;
import org.junit.Assert;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Test;
import retrofit.RetrofitError;

/**
 *
 * @author serge
 */
public class ItemTest extends CtrlTest {

    @Test
    public void testItemFetchAll() {
        System.out.println("testItemFetchAll");
        Collection<Item> list = ctrlSvc.getItemList();
        assertTrue(list != null);

    }

    @Test
    public void testItemAddFetchDelete() {
        System.out.println("testItemAddFetchDelete");
        try {
            Item random = TestData.randomItem();
            Item created = ctrlSvc.createItem(random);
            assertNotNull(created);
            List<Item> list = (List<Item>) ctrlSvc.getItemList();
            assertTrue(list.contains(created));
            ctrlSvc.deleteItem(created.getId());
        } catch (RetrofitError re) {
            //re.printStackTrace();
            fail(re.getMessage());
        }
    }

    @Test
    public void testItemAddGetByIdDelete() {
        System.out.println("testItemAddGetByIdDelete");
        try {
            Item random = TestData.randomItem();
            Item item = ctrlSvc.addItem(random);
            assertNotNull(item);
            Long id = item.getId();
            Item created = ctrlSvc.getItemById(id);
            assertNotNull(created);
            ctrlSvc.deleteItem(created.getId());
        } catch (RetrofitError re) {
            //re.printStackTrace();
            fail(re.getMessage());
        }
    }

    @Test
    public void testItemGetLatest() {
        try {
            Collection<Item> list = ctrlSvc.getItemLastChanges(null);
            assertTrue(list != null);
        } catch (RetrofitError re) {
           // re.printStackTrace();
            fail(re.getMessage());
        }
    }

    @Test
    public void testItemData() {
        try {
            Item random = TestData.randomItem();
            Item created = ctrlSvc.createItem(random);
            Assert.assertEquals(random.getName(), created.getName());
            Assert.assertEquals(random.getCode(), created.getCode());
            Assert.assertEquals(random.getPrice(), created.getPrice());
            Assert.assertEquals(random.getQuantity(), created.getQuantity());
            Assert.assertEquals(random.getUnit(), created.getUnit());
            Assert.assertEquals(random.getCategory(), created.getCategory());
            ctrlSvc.deleteItem(created.getId());
        } catch (RetrofitError re) {
          //  re.printStackTrace();
            fail();
        }

    }

}
