package ctrl;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Random;
import java.util.UUID;
import model.Client;
import model.ClientOrder;
import model.Item;
import model.OrderElement;
import org.springframework.beans.factory.annotation.Autowired;
import repo.ItemRepo;

public class TestData {
    
  

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static ClientOrder randomOrder() {
	
        
        ClientOrder random = new ClientOrder(new Date(), new Date(), null, randomClient());
        random.elements.add(randomOrderElement());
        return random;
    }

    public static Client randomClient() {
        String name = "Client " + UUID.randomUUID().toString();
        return new Client(name);
    }
    
    public static Item randomItem(){
        Random r= new Random();
        Integer code=r.nextInt();
        String name = "Item " +UUID.randomUUID().toString();
        String unit =r.toString();
        BigDecimal price = new BigDecimal(r.nextFloat());
        BigDecimal quantity = new BigDecimal(r.nextFloat());
        return new Item(name, code, unit, null, price, quantity);
    }
    
    public static OrderElement randomOrderElement(){
        Random r= new Random();
        BigDecimal price = new BigDecimal(r.nextFloat());
        BigDecimal quantity = new BigDecimal(r.nextFloat());
        return new OrderElement(quantity.intValue(),randomItem(), null);
    } 
    
    
    

    public static String toJson(Object o) throws Exception {
        return objectMapper.writeValueAsString(o);
    }
public static Object fromJson(String s,Class c) throws Exception {
        return objectMapper.readValue(s, c);
    }
    @Autowired
    private ItemRepo items;
    
}
