/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import java.util.Collection;
import java.util.Date;
import model.Category;
import model.Client;
import model.ClientOrder;
import model.Item;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 *
 * @author serge
 */
interface CtrlApi {

    public static final String PATH = "/";
    public static final String ORDERS_PATH = PATH + "order/";
    public static final String ITEMS_PATH = PATH + "item/";
    public static final String PRICES_PATH = PATH + "price/";
    public static final String CATEGORIES_PATH = PATH + "category/";
    public static final String CLIENTS_PATH = PATH + "client/";

    // Orders
    @POST(ORDERS_PATH)
    public ClientOrder addOrder(@Body ClientOrder order);

    @GET(ORDERS_PATH)
    public Collection<ClientOrder> getOrderList();

    @GET(ORDERS_PATH + "/{id}")
    public ClientOrder getOrderById(@Path("id") Long id);

    // Items
    @POST(ITEMS_PATH)
    public Item addItem(@Body Item item);

    @PUT(ITEMS_PATH)
    public Item createItem(@Body Item item);

    @DELETE(ITEMS_PATH)
    public Response deleteItem(@Query("id") Long id);

    @GET(ITEMS_PATH)
    public Collection<Item> getItemList();

    @GET(ITEMS_PATH + "{id}/")
    public Item getItemById(@Path("id") Long id);

    @GET(ITEMS_PATH)
    public Collection<Item> getItemLastChanges(@Query("lastChange") Date lastChange);

    @GET(ITEMS_PATH)
    public Item getItemByCode(@Query("code") Integer code);

    // Prices
    @POST(PRICES_PATH)
    public Item setPrice(@Body Item item);

    @DELETE(PRICES_PATH)
    public Response clearPrice(@Query("id") Long id);

    @GET(PRICES_PATH)
    public Collection<Item> getPriceList();

    @GET(PRICES_PATH + "{id}/")
    public Item getPriceById(@Path("id") Long id);

    @GET(PRICES_PATH)
    public Collection<Item> getPriceLastChanges(@Query("lastChange") Date lastChange);

    // Categories
    @POST(CATEGORIES_PATH)
    public Category addCategory(@Body Category category);

    @PUT(CATEGORIES_PATH)
    public Category createCategory(@Body Category category);

    @DELETE(CATEGORIES_PATH)
    public Response deleteCategory(@Query("id") Long id);

    @GET(CATEGORIES_PATH)
    public Collection<Category> getCategoryList();

    @GET(CATEGORIES_PATH + "{id}/")
    public Category getCategoryById(@Path("id") Long id);

    @GET(CATEGORIES_PATH)
    public Collection<Category> getCategoryLastChanges(@Query("lastChange") Date lastChange);

    @GET(CATEGORIES_PATH)
    public Category getCategoryByCode(@Query("code") Integer code);

    // Clients
    @POST(CLIENTS_PATH)
    public Client addClient(@Body Client client);

    @PUT(CLIENTS_PATH)
    public Client createClient(@Body Client client);

    @DELETE(CLIENTS_PATH)
    public Response deleteClient(@Query("id") Long id);

    @GET(CLIENTS_PATH)
    public Collection<Client> getClientList();

    @GET(CLIENTS_PATH + "{id}/")
    public Client getClientById(@Path("id") Long id);

    @GET(CLIENTS_PATH)
    public Collection<Client> getClientLastChanges(@Query("lastChange") Date lastChange);

    @GET(CLIENTS_PATH)
    public Client getClientByCode(@Query("code") Integer code);

}
