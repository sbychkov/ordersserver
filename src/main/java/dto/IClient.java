/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import model.ClientGroup;

/**
 *
 * @author serge
 */
public interface IClient {

    Integer getCode();

    Long getId();

    String getName();

    ClientGroup getParent();

    boolean isCategory();    

    void setCode(Integer code);

    void setId(Long id);

    void setName(String name);

    void setParent(ClientGroup parent);
    
}
