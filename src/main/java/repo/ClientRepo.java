/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repo;

import java.util.Date;
import java.util.List;
import model.Client;
import model.ClientGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
//import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author serge
 */
//@RepositoryRestResource(collectionResourceRel = "clients", path = "clients")
@Repository
public interface ClientRepo extends JpaRepository<Client, Long> {

    @Override
    public List<Client> findAll();

    public List<Client> findByName(@Param("name") String name);

    public List<Client> findByCode(@Param("code") Integer code);

    public List<Client> findByLastChangeAfter(@Param("lastChange") Date lastChange);
    
    public List<Client> findByParent(ClientGroup parent);
}
