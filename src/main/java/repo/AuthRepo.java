/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repo;

import model.Auth;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

/**
 *
 * @author serge
 */
@Repository
public interface AuthRepo extends JpaRepository<Auth, Long>, UserDetailsService  {

    @Override
    @Query("select a from Auth a where a.login=?1")
    public Auth loadUserByUsername(String string) throws UsernameNotFoundException;


}