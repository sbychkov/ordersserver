/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import java.text.NumberFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.logging.Logger;
import model.PriceList;
import org.springframework.beans.factory.annotation.Autowired;
import repo.CategoryRepo;
import repo.ClientGroupRepo;
import repo.ClientRepo;
import repo.DocsRepo;
import repo.ItemRepo;
import repo.OrdersRepo;
import repo.PriceListRepo;
import repo.PriceRepo;

/**
 *
 * @author serge
 */
public class BaseCtrl {
    
    protected static final Logger LOG = Logger.getLogger(ClientController.class.getName());
    
    @Autowired
    protected CategoryRepo categories;
    @Autowired
    protected ClientRepo clients;
    @Autowired
    protected ClientGroupRepo clientgroups;
    @Autowired
    protected ItemRepo items;
    @Autowired
    protected OrdersRepo orders;
    @Autowired
    protected PriceRepo prices;
    @Autowired
    protected  PriceListRepo pricelists;
    @Autowired
    protected DocsRepo debtdocs;
    
    protected boolean isNumeric(String str) {
        NumberFormat formatter = NumberFormat.getInstance();
        ParsePosition pos = new ParsePosition(0);
        formatter.parse(str, pos);
        return str.length() == pos.getIndex();
    }
    
    protected Object nl(Collection<?> c) {
        if (c != null && c.size() > 0) {
            return c.iterator().next();
        } else {
            return null;
        }
    }
    
    protected PriceList getCurrentPriceList() {
        @SuppressWarnings("deprecation")
        PriceList pl;
        pl = (PriceList) nl(pricelists.findByCreateDate());
        if (pl == null) {
            pl = pricelists.save(new PriceList(new Date()));
        }
        
        return pl;
    }
    
    protected String replaceCommaAndSpace(String str) {
        String result = "0";
        if (str != null && !str.trim().isEmpty()) {
            result = str.replace(",", ".");
        }
        return result;
    }
    
    protected Integer str2int(String str) {
        Integer result = null;
        if (str != null && !str.trim().isEmpty()) {
            result = Integer.parseInt(str.replace(",", "."));
        }
        return result;
    }
    
    protected String date2string(Date date) {
        String result = null;
        if (date != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            return sdf.format(date);
        }
        return result;
    }
    
    protected String int2code(int i, int len) {
        String format = "%0" + len + "d";
        return String.format(format, i);
    }
    
    protected Date millsStringToDate(String str) {
        Date result = new Date(0L);
        if (isNumeric(str)) {
            try {
                Long mills = Long.parseLong(str);
                result= new Date(mills);
            } catch (NumberFormatException nfe) {
                LOG.warning("Wrong number " + str);
            }
        }
        return result;
    }
    
}
