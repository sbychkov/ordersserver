/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl.file;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author serge
 */
public class FileLoop {

    private final CopyOnWriteArrayList<FileTask> taskList;
    private Thread thread;

    private FileLoop() {
        this.taskList = new CopyOnWriteArrayList<>();
    }

    public FileLoop getInstance() {
        return Inner.getLoop();
    }

    public void addTask(FileTask task) {
        taskList.add(task);
        if (thread == null) {
            startLoop();
        }
    }

    private void startLoop() {
        for (FileTask task : taskList) {
            try {
                Runnable handler = dispatch(task);

                thread = new Thread(handler, task.getHandler());
                thread.join();
            } catch (InterruptedException ex) {
                Logger.getLogger(FileLoop.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        thread = null;
    }

    private Runnable dispatch(FileTask task) {
        Runnable result = null;
        switch (task.getHandler()) {
            case "price":
                break;
            default:
                result = null;
        }
        return result;
    }

    private static class Inner {

        private static final FileLoop loop = new FileLoop();

        public static FileLoop getLoop() {

            return loop;
        }

        private Inner() {
        }
    }
}
