/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl.file;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import orders.OrdersServerApplication;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import repo.CategoryRepo;
import repo.ClientGroupRepo;
import repo.ClientRepo;
import repo.DocsRepo;
import repo.ItemRepo;
import repo.PriceListRepo;
import repo.PriceRepo;

/**
 *
 * @author 1
 */
@Configurable(autowire = Autowire.BY_TYPE)
public class BaseTask implements Runnable {

    @Autowired
    public PriceRepo prices;
    @Autowired
    public ItemRepo items;
    @Autowired
    public CategoryRepo cats;
    @Autowired
    public ClientRepo clients;
    @Autowired
    public DocsRepo docs;
    @Autowired
    public ClientGroupRepo clientgroups;
    @Autowired
    public PriceListRepo pricelists;
    @Autowired
    public ApplicationContext ctx;

    public BaseTask() {
        if (pricelists == null) {
            if (ctx == null) {
                ctx = new AnnotationConfigApplicationContext(OrdersServerApplication.class);
            }
            pricelists = ctx.getBean(PriceListRepo.class);
        }
        if (items == null) {
            if (ctx == null) {
                ctx = new AnnotationConfigApplicationContext(OrdersServerApplication.class);
            }
            items = ctx.getBean(ItemRepo.class);
        }
        if (prices == null) {
            if (ctx == null) {
                ctx = new AnnotationConfigApplicationContext(OrdersServerApplication.class);
            }
            prices = ctx.getBean(PriceRepo.class);
        }
        if (clients == null) {
            if (ctx == null) {
                ctx = new AnnotationConfigApplicationContext(OrdersServerApplication.class);
            }
            clients = ctx.getBean(ClientRepo.class);
        }
        if (cats == null) {
            if (ctx == null) {
                ctx = new AnnotationConfigApplicationContext(OrdersServerApplication.class);
            }
            cats = ctx.getBean(CategoryRepo.class);
        }
        if (clientgroups == null) {
            if (ctx == null) {
                ctx = new AnnotationConfigApplicationContext(OrdersServerApplication.class);
            }
            clientgroups = ctx.getBean(ClientGroupRepo.class);
        }
        
    }

    protected final List<String[]> strings = new ArrayList<>();

    protected String replaceCommaAndSpace(String str) {
        String result = "0";
        if (str != null && !str.trim().isEmpty()) {
            result = str.replace(",", ".");
        }
        return result;
    }

    protected Integer str2int(String str) {
        Integer result = null;
        if (str != null && !str.trim().isEmpty()) {
            result = Integer.parseInt(str.replace(",", "."));
        }
        return result;
    }

    protected String date2string(Date date) {
        String result = null;
        if (date != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            result = sdf.format(date);
        }
        return result;
    }

    protected Object nl(Collection<?> c) {
        if (c != null && c.size() > 0) {
            return c.iterator().next();
        } else {
            return null;
        }
    }

    @Override
    public void run() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
