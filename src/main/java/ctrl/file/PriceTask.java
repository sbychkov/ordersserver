/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Item;
import model.PriceList;
import model.PriceListElement;


public class PriceTask extends BaseTask {

    private static final Logger LOG = Logger.getLogger(PriceTask.class.getName());
    

    public PriceTask(List<String[]> s) {
       this();
        this.strings.addAll(s);
    }

    public PriceTask() {
        super();       
    }

    public PriceTask(File f) {
        this();
        FileReader fr = null;
        BufferedReader br = null;
        try {
            fr = new FileReader(f);
            br = new BufferedReader(fr);
            String str;
            while ((str = br.readLine()) != null) {
                String[] data = str.split("\\|", -1);
                this.strings.add(data);
            }
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, null, ex);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ex) {
                    LOG.log(Level.SEVERE, null, ex);
                }
            }
            if (fr != null) {
                try {
                    fr.close();
                } catch (IOException ex) {
                    LOG.log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    @Override
    public void run() {
        PriceList priceList = getCurrentPriceList();
        List<Item> itemsList = new ArrayList<>();
        for (String[] data : strings) {
            BigDecimal price = new BigDecimal(replaceCommaAndSpace(data[1]));
            BigDecimal quantity = new BigDecimal(replaceCommaAndSpace(data[2]));
            Integer code = str2int(data[0]);
            Item item = (Item) nl(items.findByCode(code));
            if (item != null
                    && (item.getPrice() == null
                    || (price.compareTo(item.getPrice()) != 0)
                    || item.getQuantity() == null
                    || (quantity.compareTo(item.getQuantity())) != 0)) {
                item.setQuantity(quantity);
                item.setPrice(price);
                itemsList.add(item);
            }
        }
        items.save(itemsList);
        items.flush();
        LOG.info("Items updated " + itemsList.size());
        List<PriceListElement> ples = new ArrayList<>();
        for (Item item : itemsList) {
            ples.add(new PriceListElement(item.getPrice(), item.getQuantity(), priceList, item));
        }
        prices.save(ples);
        prices.flush();
        LOG.info("PLEs updated " + ples.size());
    }

    // Needed for compability with ordersTouchkit
    //
    private PriceList getCurrentPriceList() {
        PriceList pl;
        pl = (PriceList) nl(pricelists.findByCreateDate());
        if (pl == null) {
            pl = pricelists.save(new PriceList(new Date()));
        }
        return pl;
    }

}
