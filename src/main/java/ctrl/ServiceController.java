/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.PriceListElement;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author serge
 */
@Controller
public class ServiceController extends BaseCtrl {

    @RequestMapping(value = "/updateDb", method = RequestMethod.GET)
    public String updateDb() {
        String result = " objects updated";
        int count = 0;
        Pageable pr = new PageRequest(0, 1000);
        Long prCount = prices.count();
        while (pr.getOffset() < prCount) {
            List<PriceListElement> ples = new ArrayList<>();
            for (PriceListElement ple : prices.findByCreateDateIsNull(pr)) {
                if (ple.getPriceList() != null && ple.getCreateDate() == null) {
                    ple.setCreateDate((Date) ple.getPriceList().getCreateDate()
                            .clone());
                    ples.add(ple);
                    ++count;
                }
            }
            prices.save(ples);
            pr = pr.next();
        }

        return count + result;
    }

//OPTIMIZE
    @RequestMapping(value = "/apk", method = RequestMethod.GET)
    public void getApk(HttpServletRequest request, HttpServletResponse response) {
        String fn = System.getenv("OPENSHIFT_DATA_DIR") + "app-release.apk";
        try {
            response.setContentType("application/vnd.android.package-archive");
            byte[] buffer = Files.readAllBytes(new File(fn).toPath());
            response.setContentLength(buffer.length);
            response.setHeader("Content-Transfer-Encoding", "binary");
            response.setHeader("Content-Disposition", "attachment; filename=app-release.apk");
            response.getOutputStream().write(buffer, 0, buffer.length);
        } catch (FileNotFoundException ex) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            LOG.warning("File " + fn + " not found");
        } catch (IOException ex) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            Logger.getLogger(ServiceController.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }

    }

//OPTIMIZE
    @RequestMapping(value = "/apk", method = RequestMethod.POST)
    public void postApk(@RequestParam("name") String name,
            @RequestParam("file") MultipartFile file,
            HttpServletRequest request, HttpServletResponse response) {
        if (!file.isEmpty()) {
            if (name==null||name.isEmpty()){
                name="app-release.apk";
            }
            try {
                String fn = System.getenv("OPENSHIFT_DATA_DIR") + name;//filepath
                byte[] bytes = file.getBytes();
                try (BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(fn))) {
                    stream.write(bytes);
                }
                LOG.info("File " + fn + " saved");
                response.setStatus(HttpServletResponse.SC_OK);
            }catch (IOException e){
            //catch (Exception e) {
                LOG.warning("Failed to upload " + name + " => " + e.getMessage());
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
        } else {
            LOG.warning("Failed to upload " + name + " because the file was empty.");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }
    
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public void login(HttpServletRequest request, HttpServletResponse response){
        
    }
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public void logout(HttpServletRequest request, HttpServletResponse response){
        
    }
    

}
