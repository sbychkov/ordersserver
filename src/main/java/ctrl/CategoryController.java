/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import java.util.ArrayList;
import java.util.List;
import model.Category;
import model.Item;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author serge
 */
@RestController
public class CategoryController extends BaseCtrl {

    @RequestMapping(value = "/category", method = RequestMethod.GET)
    public @ResponseBody
    List<Category> getCategories() {
        List<Category> result = new ArrayList<>();
        for(Category c:categories.findAll()){
            result.add(c.toDto());
        }
        return result;
    }

    @RequestMapping(value = "/category", method = RequestMethod.GET, params = {"lastChange"})
    public @ResponseBody
    List<Category> getLastChange(@RequestParam String lastChange) {
        return categories.findByLastChangeAfter(millsStringToDate(lastChange));
    }
    
    //OPTIMIZE
    @RequestMapping(value = "/category", method = RequestMethod.PUT)
    public @ResponseBody
    Category setCategory(@RequestBody Category cat) {
        Category dbCategory = null;
        if (cat != null) {
            if (cat.getId() != null) {
                dbCategory = categories.findOne(cat.getId());
            } else {
                dbCategory = (Category) nl(categories.findByCode(cat.getCode()));
            }
            if (dbCategory != null) {
                long id = dbCategory.getId();
                dbCategory = cat;
                dbCategory.setId(id);
                dbCategory = categories.saveAndFlush(dbCategory);
            } else {
                dbCategory = categories.saveAndFlush(cat);
            }
        }
        return dbCategory;
    }

    @RequestMapping(value = "/category", method = RequestMethod.POST)
    public @ResponseBody
    Category createCreate(@RequestBody Category cat) {
        Category dbCategory = null;
        if (cat != null) {
            dbCategory = categories.save(cat);
            items.flush();
        }
        return dbCategory;
    }

    @RequestMapping(value = "/category", method = RequestMethod.DELETE)
    public void deleteCategory(@RequestParam Long id) {
        if (id != null) {
            categories.delete(id);
        }
    }

    @RequestMapping(value = "/category/{id}", method = RequestMethod.GET)
    public @ResponseBody
    Category getCategoryById(@PathVariable("id") Long id) {
        return categories.findOne(id);
    }

    @RequestMapping(value = "/category/{id}/children", method = RequestMethod.GET)
    public @ResponseBody
    List<Category> getCategoryChildren(@PathVariable("id") Long id) {
        Category cat = categories.findOne(id);
        return cat!=null ? cat.getChildren() : null;
    }

    @RequestMapping(value = "/category/{id}/items", method = RequestMethod.GET)
    public @ResponseBody
    List<Item> getCategoryItems(@PathVariable("id") Long id) {
        Category cat = categories.findOne(id);
        return cat!=null ? items.findByCategory(cat) : null;
    }
    
}
