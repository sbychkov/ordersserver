/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class ClientOrder implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    private Long id;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Temporal(TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
    private Date estimateDate;
    //@JsonBackReference(value = "user")
    @JoinColumn(name = "USER", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;
    @JsonBackReference(value = "client-order")
    @JoinColumn(name = "CLIENT", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Client client;
    @JsonManagedReference(value = "order-oe")
    @OneToMany(mappedBy = "clientOrder", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    public List<OrderElement> elements;

    @Column(name = "executed")
    private boolean executed;
    @Column
    private BigDecimal orderSum;
    @Column
    private String docNum;
    @Column(nullable = false)
    private boolean uploaded;

    public ClientOrder() {
        this.elements = new ArrayList<>();
    }

    public ClientOrder(Date createDate, Date estimateDate, User user, Client client) {
        this.elements = new ArrayList<>();

        this.createDate = (Date) createDate.clone();
        this.estimateDate = (Date) estimateDate.clone();
        this.user = user;
        this.client = client;
    }

    public ClientOrder(Long id) {
        this.elements = new ArrayList<>();
        this.id = id;
    }

    public void setElements(List<OrderElement> elements) {
        this.elements = elements;
    }

    public List<OrderElement> getElements() {
        return elements;
    }

    public boolean isExecuted() {
        return executed;
    }

    public boolean isUploaded() {
        return uploaded;
    }

    public void setExecuted(boolean executed) {
        this.executed = executed;
    }

    public void setUploaded(boolean uploaded) {
        this.uploaded = uploaded;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateDate() {
        if (createDate != null) {
            return (Date) createDate.clone();
        } else {
            return null;
        }
    }

    public void setCreateDate(Date createdate) {
        if(createdate!=null){
        this.createDate = (Date) createdate.clone();
        }else{
            this.createDate=null;
        }
    }

    public Date getEstimateDate() {
        if (estimateDate != null) {
            return (Date) estimateDate.clone();
        } else {
            return null;
        }

    }

    public void setEstimateDate(Date estimateDate) {
        if(estimateDate !=null ){
        this.estimateDate = (Date) estimateDate.clone();
        }else{
            this.estimateDate=null;
        }
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash
                + (this.createDate != null ? this.createDate.hashCode() : 0);
        hash = 97
                * hash
                + (this.estimateDate != null ? this.estimateDate.hashCode() : 0);
        hash = 97 * hash + (this.user != null ? this.user.hashCode() : 0);
        hash = 97 * hash + (this.client != null ? this.client.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClientOrder other = (ClientOrder) obj;
        if (this.createDate != other.createDate
                && (this.createDate == null || !this.createDate
                .equals(other.createDate))) {
            return false;
        }
        if (this.estimateDate != other.estimateDate
                && (this.estimateDate == null || !this.estimateDate
                .equals(other.estimateDate))) {
            return false;
        }
        if (this.user != other.user
                && (this.user == null || !this.user.equals(other.user))) {
            return false;
        }
        if (this.client != other.client
                && (this.client == null || !this.client.equals(other.client))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ClientOrder{" + "id=" + id + ", createDate=" + createDate
                + ", estimateDate=" + estimateDate + ", user=" + user
                + ", client=" + client + '}';
    }

    @JsonIgnore
    public BigDecimal getSum() {
        BigDecimal result = BigDecimal.ZERO;

        for (OrderElement oe : elements) {
            if (oe.getQuantity() != null && oe.getElementPrice() != null) {
                result = result.add(oe.getElementPrice()
                        .multiply(new BigDecimal(oe.getQuantity())));
            }
        }
        this.orderSum = result;
        return result;
    }

    /**
     * @return the orderSum
     */
    public BigDecimal getOrderSum() {
        return orderSum;
    }

    /**
     * @param orderSum the orderSum to set
     */
    public void setOrderSum(BigDecimal orderSum) {
        this.orderSum = orderSum;
    }

    /**
     * @return the docNum
     */
    public String getDocNum() {
        return docNum;
    }

    /**
     * @param docNum the docNum to set
     */
    public void setDocNum(String docNum) {
        this.docNum = docNum;
    }

}
