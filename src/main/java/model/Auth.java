/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@XmlRootElement
public class Auth implements Serializable,EntityId,UserDetails {
	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Long id;

	@Column(length = 255)
	private String login;

	@Column(length = 255)
	private String pass;
	@OneToMany(mappedBy = "auth")
	private List<User> userList;

        @Column
        private boolean enabled;
        @Column
        private boolean accountExpiried;
        @Column
        private boolean credentialsExpiried;
        @Column
        private boolean accountLocked;
        
	public Auth() {
        this.enabled = true;
	}

	public Auth(String login, String pass) {
        this.enabled = true;
		this.login = login;
		this.pass = pass;
	}

	public Auth(Long id) {
        this.enabled = true;
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	@XmlTransient
	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	@Override
	public String toString() {
		return "Auth{" + "id=" + id + '}';
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 89 * hash + (this.login != null ? this.login.hashCode() : 0);
		hash = 89 * hash + (this.pass != null ? this.pass.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Auth other = (Auth) obj;
		if ((this.login == null) ? (other.login != null) : !this.login
				.equals(other.login)) {
			return false;
		}
		if ((this.pass == null) ? (other.pass != null) : !this.pass
				.equals(other.pass)) {
			return false;
		}
		return true;
	}

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
       return pass;
    }

    @Override
    public String getUsername() {
        return login;
    }

    @Override
    public boolean isAccountNonExpired() {
     return !accountExpiried;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !accountLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
       return !credentialsExpiried;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

}
