/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Category implements Serializable, Entity1c, EntityAudit {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private Integer code;
    //@JsonBackReference(value="category")
    @ManyToOne
    private Category parent;
    //@JsonManagedReference(value="category")
    @JsonIgnore
    @OneToMany(mappedBy = "parent")
    private List<Category> children;
    @JsonIgnore
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @JsonIgnore
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastChange;

    public Category() {
    }

    public Category(String name, Category parent) {
        this.name = name;
        this.parent = parent;
    }

    public Category(String name, Integer code, Category parent) {
        this.name = name;
        this.parent = parent;
        this.code = code;
    }

    public Category(String name, Integer code) {
        this.name = name;
        this.code = code;
        this.parent = null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the parent
     */
    public Category getParent() {
        return parent;
    }

    /**
     * @param parent the parent to set
     */
    public void setParent(Category parent) {
        this.parent = parent;
    }

    /**
     * @return the children
     */
    public List<Category> getChildren() {
        return children;
    }

    /**
     * @param children the children to set
     */
    public void setChildren(List<Category> children) {
        this.children = children;
    }

    /**
     * @return the code
     */
    public Integer getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(Integer code) {
        this.code = code;
    }

    @PrePersist
    @Override
    public void precreate() {
        createDate = new Date();
    }

    @PreUpdate
    @Override
    public void preedit() {

        lastChange = new Date();
        if (createDate == null) {
            createDate = lastChange;
        }
    }

    /**
     * @return the createDate
     */
    @Override
    public Date getCreateDate() {
        if (createDate != null) {
            return (Date) createDate.clone();
        } else {
            return null;
        }
    }

    /**
     * @param createDate the createDate to set
     */
    @Override
    public void setCreateDate(Date createDate) {
        if(createDate!=null){
        this.createDate = (Date) createDate.clone();
        }else{
            this.createDate=null;
        }
    }

    /**
     * @return the lastChange
     */
    @Override
    public Date getLastChange() {
        if (lastChange != null) {
            return (Date) lastChange.clone();
        } else {
            return null;
        }
    }

    /**
     * @param lastChange the lastChange to set
     */
    @Override
    public void setLastChange(Date lastChange) {
        if(lastChange!=null){
        this.lastChange = (Date) lastChange.clone();
        }else{
            this.lastChange=null;
        }
    }

    public Category toDto() {
        Category result = new Category(name, code);
        if (parent != null) {
            result.setParent(new Category(parent.getName(), parent.getCode()));
        }
        return result;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.name);
        hash = 71 * hash + Objects.hashCode(this.code);
        hash = 71 * hash + Objects.hashCode(this.parent);
        hash = 71 * hash + Objects.hashCode(this.children);
        hash = 71 * hash + Objects.hashCode(this.createDate);
        hash = 71 * hash + Objects.hashCode(this.lastChange);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Category other = (Category) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.parent, other.parent)) {
            return false;
        }
        if (!Objects.equals(this.children, other.children)) {
            return false;
        }
        if (!Objects.equals(this.createDate, other.createDate)) {
            return false;
        }
        if (!Objects.equals(this.lastChange, other.lastChange)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Category{" + "id=" + id + ", name=" + name + ", code=" + code + ", parent=" + parent + ", children=" + children + ", createDate=" + createDate + ", lastChange=" + lastChange + '}';
    }

    
    
}
