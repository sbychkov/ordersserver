/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import dto.IClientGroup;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author serge
 */
@Entity
public class ClientGroup implements Serializable, IClientGroup {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private Integer code;
    ///@JsonBackReference("parent")
    @ManyToOne
    private ClientGroup parent;
    @JsonManagedReference("parent")
    @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
    private List<ClientGroup> children;

    public ClientGroup() {
    }

    public ClientGroup(String name, Integer code) {
        this.name = name;
        this.code = code;
    }

    public ClientGroup(String name, Integer code, ClientGroup parent) {
        this.name = name;
        this.code = code;
        this.parent = parent;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    @Override
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the code
     */
    @Override
    public Integer getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    @Override
    public void setCode(Integer code) {
        this.code = code;
    }

    /**
     * @return the parent
     */
    @Override
    public ClientGroup getParent() {
        return parent;
    }

    /**
     * @param parent the parent to set
     */
    @Override
    public void setParent(ClientGroup parent) {
        this.parent = parent;
    }

    /**
     * @return the children
     */
    @JsonIgnore
    public List<ClientGroup> getChildren() {
        return children;
    }

    /**
     * @param children the children to set
     */
    public void setChildren(List<ClientGroup> children) {
        this.children = children;
    }

    @Override
    public boolean isCategory() {
        return true;
    }

    public ClientGroup toDto() {
        ClientGroup result = new ClientGroup(name, code);
        if (parent != null) {
            result.setParent(new ClientGroup(parent.getName(), parent.getCode()));
        }
        return result;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.name);
        hash = 47 * hash + Objects.hashCode(this.code);
        hash = 47 * hash + Objects.hashCode(this.parent);
        hash = 47 * hash + Objects.hashCode(this.children);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClientGroup other = (ClientGroup) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        if (!Objects.equals(this.parent, other.parent)) {
            return false;
        }
        if (!Objects.equals(this.children, other.children)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ClientGroup{" + "id=" + id + ", name=" + name + ", code=" + code + ", parent=" + parent + ", children=" + children + '}';
    }

    
    
}
